﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager
{
    [DataContract]
    public class LocalPayment
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [DataMember(Name = "value")]
        public double Value { get; set; }

        [DataMember(Name = "movement_start_time")]
        public string MovementDate { get; set; }

        [DataMember(Name = "owner_username")]
        public string OwnerUsername { get; set; }

        [DataMember(Name = "date")]
        public string PaymentDate { get; set; }

        /*[DataMember(Name = "synchronized")]
        public bool Synchronized { get; set; }*/
    }
}
