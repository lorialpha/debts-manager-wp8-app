﻿using Debts_Manager.Synchronization;
using SQLite;
using SQLite.Net;
using SQLite.Net.Async;
using SQLite.Net.Platform.WinRT;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace Debts_Manager
{
    public sealed class LocalStorageManager
    {
        private const string SQLITE_FILE = "debtsmgr.db";
        private const string DB_DEF_FILE = "dbdef.sql";

        public async static Task createStorage() {
            if( await dbExists() ) {
                return;
            }

            using (var conn = createConnection())
            {
                SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                Task v = asyncConn.RunInTransactionAsync(tran =>
                {
                    tran.CreateTable<LocalMovement>();
                    tran.CreateTable<LocalPayment>();
                    tran.CreateTable<SynchronizationMovementCreation>();
                    tran.CreateTable<SynchronizationMovementDone>();
                    tran.CreateTable<SynchronizationPaymentCreation>();
                });

                await v;
            }
        }

        public async static Task deleteStorage()
        {
            if (await dbExists())
            {
                StorageFile file = await ApplicationData.Current.LocalFolder.GetFileAsync(SQLITE_FILE);
                await file.DeleteAsync();
            }
        }

        public async static Task recreateStorage()
        {
            await deleteStorage();
            await createStorage();
        }

        public async static Task<int> addMovement(LocalMovement movement) {
            try
            {
                using (var conn = createConnection())
                {
                    ICollection<int> getResultHere = new List<int>(1);
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Insert(movement);

                        movement = tran.Table<LocalMovement>().Where(x => x.Owner == movement.Owner && x.Date == movement.Date).First();
                        getResultHere.Add(movement.Id);
                    });

                    await v;
                    if( v.IsFaulted || getResultHere.Count != 1)
                    {
                        Debug.WriteLine(v.Exception);
                        return -1;
                    }

                    return getResultHere.First();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return -1;
            }
        }

        public async static Task<int> addPayment(LocalPayment payment)
        {
            try
            {
                using (var conn = createConnection())
                {
                    ICollection<int> getResultHere = new List<int>(1);
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Insert(payment);

                        payment = tran.Table<LocalPayment>().Where(x => x.OwnerUsername == payment.OwnerUsername && x.MovementDate == payment.MovementDate && x.PaymentDate == payment.PaymentDate).First();

                        getResultHere.Add(payment.Id);
                    });

                    await v;
                    if (v.IsFaulted || getResultHere.Count != 1)
                    {
                        Debug.WriteLine(v.Exception);
                        return -1;
                    }

                    return getResultHere.First();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return -1;
            }
        }

        public async static Task<int> addPaymentAndUpdateMovement(LocalPayment payment)
        {
            try
            {
                using (var conn = createConnection())
                {
                    ICollection<int> getResultHere = new List<int>(1);
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Insert(payment);

                        payment = tran.Table<LocalPayment>().Where(x => x.OwnerUsername == payment.OwnerUsername && x.MovementDate == payment.MovementDate && x.PaymentDate == payment.PaymentDate).First();

                        getResultHere.Add(payment.Id);

                        LocalMovement movement = tran.Table<LocalMovement>().Where(x => x.Owner == payment.OwnerUsername && x.Date == payment.MovementDate).First();

                        movement.CurrentValue += payment.Value;

                        tran.Update(movement);

                        movement = tran.Table<LocalMovement>().Where(x => x.Owner == payment.OwnerUsername && x.Date == payment.MovementDate).First();
                    });

                    await v;
                    if (v.IsFaulted || getResultHere.Count != 1)
                    {
                        Debug.WriteLine(v.Exception);
                        return -1;
                    }

                    return getResultHere.First();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return -1;
            }
        }
        
        public async static Task<bool> updatePayment(LocalPayment payment)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Update(payment);

                    });

                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }
        
        public async static Task<bool> updateMovement(LocalMovement movement)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Update(movement);
                    });

                    await v;
                    if (v.IsFaulted)
                    {
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> updateMovementTimestampAndRemoveCreationEvent(LocalMovement movement, string timestamp, int eventId)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    await asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Table<SynchronizationMovementCreation>().Delete(x => x.Id == eventId);

                        if (!movement.Date.Equals(timestamp))
                        {
                            string prevDate = movement.Date;
                            movement.Date = timestamp;
                            tran.Update(movement);

                            List<LocalPayment> payments = tran.Table<LocalPayment>().Where(x => x.MovementDate == prevDate && x.OwnerUsername == movement.Owner).ToList();

                            foreach (LocalPayment payment in payments)
                            {
                                payment.MovementDate = timestamp;
                            }

                            tran.UpdateAll(payments);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> updatePaymentTimestampAndRemoveCreationEvent(LocalPayment payment, string timestamp, int eventId)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    await asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Table<SynchronizationPaymentCreation>().Delete(x => x.Id == eventId);

                        if (!payment.PaymentDate.Equals(timestamp))
                        {
                            string prevDate = payment.PaymentDate;
                            payment.PaymentDate = timestamp;
                            tran.Update(payment);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> UpdateMovementAndAddDoneSynchronizationEvent(LocalMovement movement)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    await asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Update(movement);
                        SynchronizationMovementDone eventDone = tran.Table<SynchronizationMovementDone>().Where(x => x.MovementId == movement.Id).FirstOrDefault();
                        if (eventDone == null)
                        {
                            tran.Insert(new SynchronizationMovementDone
                            {
                                MovementId = movement.Id
                            });
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> UpdateMovementAndAddDoneSynchronizationEvent(int movementId, bool doneValue)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        LocalMovement movement = tran.Table<LocalMovement>().Where(x => x.Id == movementId).FirstOrDefault();
                        if (movement == null)
                        {
                            throw new NullReferenceException();
                        }

                        movement.Done = doneValue;
                        tran.Update(movement);
                        SynchronizationMovementDone eventDone = tran.Table<SynchronizationMovementDone>().Where(x => x.MovementId == movement.Id).FirstOrDefault();
                        if (eventDone == null)
                        {
                            tran.Insert(new SynchronizationMovementDone
                            {
                                MovementId = movement.Id
                            });
                        }
                    });
                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> AddPaymentAndAddCreationSynchronizationEvent(int movementId, double value, DateTime paymentDate)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        LocalMovement movement = tran.Table<LocalMovement>().Where(x => x.Id == movementId).FirstOrDefault();
                        if (movement == null)
                        {
                            throw new NullReferenceException();
                        }

                        movement.CurrentValue += value;
                        tran.Update(movement);

                        string dateAsString = App.DateTimeToDateString(paymentDate);
                        tran.Insert(new LocalPayment
                        {
                            Value = value,
                            MovementDate = movement.Date,
                            OwnerUsername = movement.Owner,
                            PaymentDate = dateAsString
                        });

                        LocalPayment insertedPay = tran.Table<LocalPayment>().Where(x => x.MovementDate == movement.Date &&
                            x.OwnerUsername == movement.Owner &&
                            x.PaymentDate == dateAsString).First();

                        tran.Insert(new SynchronizationPaymentCreation
                        {
                            MovementId = movementId,
                            PaymentId = insertedPay.Id
                        });
                    });
                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> AddMovementAndAddCreationSynchronizationEvent(string owner, string title, double value, string subject, DateTime movementDate)
        {
            Debug.WriteLine(movementDate);

            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Insert(new LocalMovement
                        {
                            Owner = owner,
                            Title = title,
                            Date = App.DateTimeToDateString(movementDate),
                            Value = value,
                            CurrentValue = value,
                            SubjectName = subject,
                            Done = false
                        });

                        string movementDateAsString = App.DateTimeToDateString(movementDate);
                        LocalMovement insertedMov = tran.Table<LocalMovement>().Where(x => x.Owner == owner && x.Date == movementDateAsString).First();

                        tran.Insert(new SynchronizationMovementCreation
                        {
                            MovementId = insertedMov.Id
                        });
                    });
                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<ICollection<LocalMovement>> getAllMovements()
        {

            try
            {
                using (var conn = createConnection())
                {
                    List<LocalMovement> result = new List<LocalMovement>();
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        result.AddRange(tran.Table<LocalMovement>().OrderByDescending(x => x.Date));
                    });

                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return null;
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<ICollection<LocalMovement>> getMovementsWithDate(DateTime startDate, DateTime endDate)
        {

            try
            {
                using (var conn = createConnection())
                {
                    List<LocalMovement> result = new List<LocalMovement>();
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    string startDateStr = App.DateTimeToDateString(startDate);
                    string endDateStr = App.DateTimeToDateString(endDate);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        result.AddRange(tran.Query<LocalMovement>("SELECT * FROM LocalMovement WHERE Date >= ? AND Date <= ?", startDateStr, endDateStr));
                        //result.AddRange(tran.Table<LocalMovement>().Where(x => string.Compare(x.Date, startDateStr) >= 0 && string.Compare(x.Date, endDateStr) <= 0).OrderByDescending(x => x.Date));
                    });

                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return null;
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<ICollection<LocalPayment>> getAllPayments(string movementOwner, DateTime movementDate)
        {
            string movementDateAsString = App.DateTimeToDateString(movementDate);
            return await getAllPayments(movementOwner, movementDateAsString);
        }

        public async static Task<ICollection<LocalPayment>> getAllPayments(string movementOwner, string movementDate)
        {
            try
            {
                using (var conn = createConnection())
                {
                    List<LocalPayment> result = new List<LocalPayment>();
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        result.AddRange(tran.Table<LocalPayment>().Where(p => p.OwnerUsername == movementOwner && p.MovementDate == movementDate)
                            .OrderByDescending(x => x.PaymentDate));
                    });

                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return null;
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<ICollection<LocalMovement>> getNotDoneMovements()
        {
            try
            {
                using (var conn = createConnection())
                {
                    List<LocalMovement> result = new List<LocalMovement>();
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        result.AddRange(tran.Table<LocalMovement>().Where(x => x.Done == false).OrderByDescending(x => x.Date));
                    });

                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return null;
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<ICollection<LocalMovement>> getDoneMovements()
        {
            try
            {
                using (var conn = createConnection())
                {
                    List<LocalMovement> result = new List<LocalMovement>();
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        result.AddRange(tran.Table<LocalMovement>().Where(x => x.Done == true).OrderByDescending(x => x.Date));
                    });
                   
                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return null;
                    }

                    return result;
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<LocalMovement> getMovement(int id)
        {
            try
            {
                using (var conn = createConnection())
                {
                    List<LocalMovement> result = new List<LocalMovement>();
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        result.Add(tran.Table<LocalMovement>().Where(x => x.Id == id).First());
                    });
                   
                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return null;
                    }

                    return result.First();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<LocalPayment> getPayment(int id)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    return await asyncConn.Table<LocalPayment>().Where(x => x.Id == id).FirstOrDefaultAsync();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<ICollection<SynchronizationMovementCreation>> getMovementCreationEvents()
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    return await asyncConn.Table<SynchronizationMovementCreation>().ToListAsync();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<ICollection<SynchronizationPaymentCreation>> getPaymentCreationEvents()
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    return await asyncConn.Table<SynchronizationPaymentCreation>().ToListAsync();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<ICollection<SynchronizationMovementDone>> getMovementSetDoneEvents()
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    return await asyncConn.Table<SynchronizationMovementDone>().ToListAsync();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return null;
            }
        }

        public async static Task<bool> deleteMovementCreationEvent(int id)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Table<SynchronizationMovementCreation>().Delete(x => x.Id == id);
                    });


                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        public async static Task<bool> deletePaymentCreationEvent(int id)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Table<SynchronizationPaymentCreation>().Delete(x => x.Id == id);
                    });

                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        public async static Task<bool> deleteMovementDoneEvent(int id)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    Task v = asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Table<SynchronizationMovementDone>().Delete(x => x.Id == id);
                    });

                    await v;
                    if (v.IsFaulted)
                    {
                        Debug.WriteLine(v.Exception);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        public async static Task<bool> deletePaymentCreationAndStorageData(LocalPayment payment, SynchronizationPaymentCreation creationEvent)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    await asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Delete<SynchronizationPaymentCreation>(creationEvent);

                        LocalMovement localMov = tran.Table<LocalMovement>().Where(x => x.Id == creationEvent.MovementId).FirstOrDefault();

                        tran.Delete<LocalPayment>(payment);

                        localMov.Value -= payment.Value;

                        tran.Update(localMov);
                    });

                }

                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        public async static Task<bool> deleteMovementCreationPaymentsAndDoneEventsAndStorageData(int idOfMovement)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    await asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Table<SynchronizationMovementCreation>().Delete(x => x.MovementId == idOfMovement);

                        tran.Table<SynchronizationMovementDone>().Delete(x => x.MovementId == idOfMovement);

                        tran.Table<SynchronizationPaymentCreation>().Delete(x => x.MovementId == idOfMovement);

                        LocalMovement localMov = tran.Table<LocalMovement>().Where(x => x.Id == idOfMovement).FirstOrDefault();

                        tran.Table<LocalPayment>().Delete(x => x.MovementDate == localMov.Date && x.OwnerUsername == localMov.Owner);

                        tran.Delete<LocalMovement>(localMov);
                    });
                    
                }

                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        public async static Task<bool> deleteMovementCreationPaymentsAndDoneEventsAndStorageData(LocalMovement movement)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    await asyncConn.RunInTransactionAsync(tran =>
                    {
                        tran.Table<SynchronizationMovementCreation>().Delete(x => x.MovementId == movement.Id);

                        tran.Table<SynchronizationMovementDone>().Delete(x => x.MovementId == movement.Id);

                        tran.Table<SynchronizationPaymentCreation>().Delete(x => x.MovementId == movement.Id);

                        tran.Table<LocalPayment>().Delete(x => x.MovementDate == movement.Date && x.OwnerUsername == movement.Owner);

                        tran.Delete<LocalMovement>(movement);
                    });

                }

                return true;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }
        }

        public async static Task<bool> deleteMovement(string owner, DateTime movementDate)
        {
            try
            {
                using (var conn = createConnection())
                {
                    string movementDateAsString = App.DateTimeToDateString(movementDate);
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    LocalMovement mov = await asyncConn.Table<LocalMovement>().Where(x => x.Owner == owner && x.Date == movementDateAsString).FirstOrDefaultAsync();
                    await asyncConn.DeleteAsync<LocalMovement>(mov);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> deleteMovement(LocalMovement movement)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    await asyncConn.DeleteAsync<LocalMovement>(movement);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> deletePayment(string owner, DateTime movementDate, DateTime paymentDate)
        {
            try
            {
                using (var conn = createConnection())
                {
                    string movementDateAsString = App.DateTimeToDateString(movementDate);
                    string paymentDateAsString = App.DateTimeToDateString(paymentDate);
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    LocalPayment pay = await asyncConn.Table<LocalPayment>().Where(x => x.OwnerUsername == owner && x.MovementDate == movementDateAsString && x.PaymentDate == paymentDateAsString).FirstOrDefaultAsync();
                    await asyncConn.DeleteAsync<LocalPayment>(pay);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> deletePayment(LocalPayment payment)
        {
            try
            {
                using (var conn = createConnection())
                {
                    SQLiteAsyncConnection asyncConn = new SQLiteAsyncConnection(() => conn);
                    await asyncConn.DeleteAsync<LocalPayment>(payment);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                return false;
            }

            return true;
        }

        public async static Task<bool> dbExists()
        {
            try
            {
                await ApplicationData.Current.LocalFolder.GetFileAsync(SQLITE_FILE);
                return true;
            }
            catch (FileNotFoundException)
            {
                return false;
            }

        }

        /*
         * http://techreadme.blogspot.it/2012/11/sqlite-read-write-datetime-values-using.html
         */
        private static string DateTimeSQLite(DateTime datetime)
        {
            datetime = datetime.ToUniversalTime();
            string dateTimeFormat = "{0}-{1}-{2} {3}:{4}:{5}.{6}";
            return string.Format(dateTimeFormat, datetime.Year, datetime.Month, datetime.Day, datetime.Hour, datetime.Minute, datetime.Second, datetime.Millisecond);
        }

        private static ICollection<string> splitQueries(string ddl)
        {
            ICollection<string> result = new LinkedList<string>();
            string[] queries = ddl.Split(new char[] {';'});

            foreach( string query in queries ) {
                string qu = query.Trim();
                if (qu.Length == 0)
                {
                    continue;
                }

                result.Add(qu);
            }

            return result;
        }

        private static SQLiteConnectionWithLock createConnection() {
            SQLiteConnectionWithLock conn = new SQLiteConnectionWithLock(new SQLitePlatformWinRT(), new SQLiteConnectionString(Path.Combine(Path.Combine(ApplicationData.Current.LocalFolder.Path, SQLITE_FILE)), storeDateTimeAsTicks: true));
            //var conn = new SQLiteConnection(new SQLitePlatformWinRT(), Path.Combine(Path.Combine(ApplicationData.Current.LocalFolder.Path, SQLITE_FILE)));
            return conn;
        }
    }
}
