﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager
{
    [DataContract]
    public class Payment
    {
        [DataMember(Name = "value")]
        public double Value { get; set; }

        [DataMember(Name = "movement_start_time")]
        public DateTime MovementDate { get; set; }

        [DataMember(Name="owner_username")]
        public string OwnerUsername { get; set; }

        [DataMember(Name="date")]
        public DateTime PaymentDate { get; set; }
    }
}
