﻿using Debts_Manager.Synchronization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Storage.Streams;
using Windows.Web.Http;
using Windows.Web.Http.Filters;
using Windows.Web.Http.Headers;

namespace Debts_Manager
{

    public sealed class SynchronizationManager
    {
        public enum SynchronizationSubject {
            MOVEMENT_CREATION, MOVEMENT_CHANGE, PAYMENT_CREATION, MOVEMENT_DELETED, PAYMENT_DELETED
        }

        public delegate void SynchronizationObserver(SynchronizationSubject subject, object data);

        private const int MAX_SERVER_ERRORS = 3;
        private readonly SemaphoreSlim syncThreadSpawnSem = new SemaphoreSlim(1);
        //private volatile bool operationEnded = false;
        private object operationResult = null;
        private object operationResultEx = null;
        //private readonly object operationEndCondition = new object();
        private AsyncBarrier operationBarrier = null;

        private readonly App app;
        private IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> httpOperation = null;
        private volatile IAsyncAction asyncThread = null;
        private volatile bool relaunchSynchronization = false;
        public event SynchronizationObserver OnSynchronized;

        public SynchronizationManager(App app)
        {
            this.app = app;
        }

        public async void startSync()
        {
            if (syncThreadSpawnSem.CurrentCount <= 0)
            {
                relaunchSynchronization = true;
                return;
            }

            await syncThreadSpawnSem.WaitAsync();
            try
            {
                relaunchSynchronization = false;
                asyncThread = Windows.System.Threading.ThreadPool.RunAsync(synchronize);
                asyncThread.Completed = manageSynchronizationEnd;
            }
            finally
            {
                syncThreadSpawnSem.Release();
            }
        }

        public void stopSync()
        {
            relaunchSynchronization = false;
            cancelCurrentTask();
        }

        public async Task recreateStorage()
        {
            cancelCurrentTask();

            await syncThreadSpawnSem.WaitAsync();
            try
            {
                await LocalStorageManager.recreateStorage();
            }
            finally
            {
                syncThreadSpawnSem.Release();
            }
        }

        public async Task createStorage()
        {
            cancelCurrentTask();

            await syncThreadSpawnSem.WaitAsync();
            try
            {
                await LocalStorageManager.createStorage();
            }
            finally
            {
                syncThreadSpawnSem.Release();
            }
        }

        public async Task deleteStorage()
        {
            cancelCurrentTask();

            await syncThreadSpawnSem.WaitAsync();
            try
            {
                await LocalStorageManager.deleteStorage();
            }
            finally
            {
                syncThreadSpawnSem.Release();
            }
        }

        public async Task<bool> SetMovementDone(LocalMovement movement)
        {
            if (await LocalStorageManager.UpdateMovementAndAddDoneSynchronizationEvent(movement))
            {
                startSync();
                return true;
            }

            return false;
        }

        public async Task<bool> SetMovementDone(int movementId, bool doneValue)
        {
            if (await LocalStorageManager.UpdateMovementAndAddDoneSynchronizationEvent(movementId, doneValue))
            {
                startSync();
                return true;
            }

            return false;
        }

        public async Task<bool> AddPayment(int movementId, double value, DateTime paymentDate) 
        {
            if (await LocalStorageManager.AddPaymentAndAddCreationSynchronizationEvent(movementId, value, paymentDate))
            {
                startSync();
                return true;
            }

            return false;
        }

        public async Task<bool> AddMovement(string owner, string title, double value, string subject, DateTime date)
        {
            if (await LocalStorageManager.AddMovementAndAddCreationSynchronizationEvent(owner, title, value, subject, date))
            {
                startSync();
                return true;
            }

            return false;
        }

        private void cancelCurrentTask()
        {
            IAsyncAction asyncTask = asyncThread;

            if (asyncTask != null)
            {
                if (asyncTask.Status != AsyncStatus.Canceled && asyncTask.Status != AsyncStatus.Completed && asyncTask.Status != AsyncStatus.Error)
                {
                    asyncTask.Cancel();
                }
            }
        }

        private void manageSynchronizationEnd(IAsyncInfo asyncInfo, AsyncStatus asyncStatus)
        {
            if (relaunchSynchronization)
            {
                startSync();
            }
        }

        private async void manageMovementAddEnd(IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> asyncInfo, AsyncStatus asyncStatus)
        {
            bool releaseOperation = false;
            switch (asyncStatus)
            {
                case AsyncStatus.Started:
                    break;
                case AsyncStatus.Completed:
                    HttpResponseMessage response = asyncInfo.GetResults();
                    if (response.IsSuccessStatusCode)
                    {
                        try
                        {
                            operationResult = AddMovementResult.SUCCESS;
                            operationResultEx = JsonConvert.DeserializeObject<SynchronizationMovementTimestamp>(await response.Content.ReadAsStringAsync());
                        }
                        catch (Exception)
                        {
                            operationResult = AddMovementResult.SERVER_ERROR;
                            operationResultEx = null;
                        }
                    }
                    else if (response.StatusCode == HttpStatusCode.Conflict)
                    {
                        operationResult = AddMovementResult.CONFLICT;
                    }
                    else
                    {
                        operationResult = AddMovementResult.NET_ERROR;
                    }

                    response.Dispose();
                    releaseOperation = true;
                    break;
                case AsyncStatus.Canceled:
                case AsyncStatus.Error:
                    operationResult = AddMovementResult.NET_ERROR;
                    releaseOperation = true;
                    break;
            }

            if (releaseOperation)
            {
                await operationBarrier.SignalAndWait();
                /*lock (operationEndCondition)
                {
                    operationEnded = true;
                    Monitor.PulseAll(operationEndCondition);
                }*/
            }
        }

        private async void manageMovementSetDone(IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> asyncInfo, AsyncStatus asyncStatus)
        {
            bool releaseOperation = false;
            switch (asyncStatus)
            {
                case AsyncStatus.Started:
                    break;
                case AsyncStatus.Completed:
                    HttpResponseMessage response = asyncInfo.GetResults();
                    if (response.IsSuccessStatusCode)
                    {
                        operationResult = SetMovementDoneResult.SUCCESS;
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        operationResult = SetMovementDoneResult.NOT_FOUND;
                    }
                    else
                    {
                        operationResult = SetMovementDoneResult.NET_ERROR;
                    }

                    response.Dispose();
                    releaseOperation = true;
                    break;
                case AsyncStatus.Canceled:
                case AsyncStatus.Error:
                    operationResult = SetMovementDoneResult.NET_ERROR;
                    releaseOperation = true;
                    break;
            }

            if (releaseOperation)
            {
                await operationBarrier.SignalAndWait();
                /*lock (operationEndCondition)
                {
                    operationEnded = true;
                    Monitor.PulseAll(operationEndCondition);
                }*/
            }
        }

        private async void managePaymentAdd(IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> asyncInfo, AsyncStatus asyncStatus)
        {
            bool releaseOperation = false;
            switch (asyncStatus)
            {
                case AsyncStatus.Started:
                    break;
                case AsyncStatus.Completed:
                    HttpResponseMessage response = asyncInfo.GetResults();
                    if (response.IsSuccessStatusCode)
                    {
                        try
                        {
                            operationResult = AddPaymentResult.SUCCESS;
                            operationResultEx = JsonConvert.DeserializeObject<SynchronizationPaymentTimestamp>(await response.Content.ReadAsStringAsync());

                        }
                        catch (Exception)
                        {
                            operationResult = AddPaymentResult.SERVER_ERROR;
                            operationResultEx = null;
                        }
                    }
                    else if (response.StatusCode == HttpStatusCode.NotFound)
                    {
                        operationResult = AddPaymentResult.NOT_FOUND;
                    }
                    else if (response.StatusCode == HttpStatusCode.Conflict)
                    {
                        operationResult = AddPaymentResult.CONFLICT;
                    }
                    else
                    {
                        operationResult = AddPaymentResult.NET_ERROR;
                    }

                    response.Dispose();
                    releaseOperation = true;
                    break;
                case AsyncStatus.Canceled:
                case AsyncStatus.Error:
                    operationResult = AddPaymentResult.NET_ERROR;
                    releaseOperation = true;
                    break;
            }

            if (releaseOperation)
            {
                await operationBarrier.SignalAndWait();
                /*lock (operationEndCondition)
                {
                    operationEnded = true;
                    Monitor.PulseAll(operationEndCondition);
                }*/
            }
        }

        private async void manageMovementTimestamp(IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> asyncInfo, AsyncStatus asyncStatus)
        {
            bool releaseOperation = false;
            switch (asyncStatus)
            {
                case AsyncStatus.Started:
                    break;
                case AsyncStatus.Completed:
                    HttpResponseMessage response = asyncInfo.GetResults();
                    if (response.IsSuccessStatusCode)
                    {
                        try
                        {
                            try
                            {
                                string responseStr = await response.Content.ReadAsStringAsync();
                                Debug.WriteLine("Movement timestamps: " + responseStr);
                                operationResult = JsonConvert.DeserializeObject<List<SynchronizationMovementTimestamp>>(responseStr);
                            }
                            catch (Exception)
                            {
                                operationResult = null;
                            }
                        }
                        catch (Exception)
                        {
                            operationResult = null;
                        }
                    }
                    else
                    {
                        operationResult = null;
                    }

                    response.Dispose();
                    releaseOperation = true;
                    break;
                case AsyncStatus.Canceled:
                case AsyncStatus.Error:
                    operationResult = null;
                    releaseOperation = true;
                    break;
            }

            if (releaseOperation)
            {
                await operationBarrier.SignalAndWait();
                /*lock (operationEndCondition)
                {
                    operationEnded = true;
                    Monitor.PulseAll(operationEndCondition);
                }*/
            }
        }

        private async void manageMovementPayments(IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> asyncInfo, AsyncStatus asyncStatus)
        {
            bool releaseOperation = false;
            switch (asyncStatus)
            {
                case AsyncStatus.Started:
                    break;
                case AsyncStatus.Completed:
                    HttpResponseMessage response = asyncInfo.GetResults();
                    if (response.IsSuccessStatusCode)
                    {
                        try
                        {
                            string responseStr = await response.Content.ReadAsStringAsync();

                            operationResult = JsonConvert.DeserializeObject<List<SynchronizationMovementPayment>>(responseStr);
                        }
                        catch (Exception)
                        {
                            operationResult = null;
                        }
                    }
                    else
                    {
                        operationResult = null;
                    }

                    response.Dispose();
                    releaseOperation = true;
                    break;
                case AsyncStatus.Canceled:
                case AsyncStatus.Error:
                    operationResult = null;
                    releaseOperation = true;
                    break;
            }

            if (releaseOperation)
            {
                await operationBarrier.SignalAndWait();
                /*lock (operationEndCondition)
                {
                    operationEnded = true;
                    Monitor.PulseAll(operationEndCondition);
                }*/
            }
        }

        private async void manageMovementDetails(IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> asyncInfo, AsyncStatus asyncStatus)
        {
            bool releaseOperation = false;
            switch (asyncStatus)
            {
                case AsyncStatus.Started:
                    break;
                case AsyncStatus.Completed:
                    HttpResponseMessage response = asyncInfo.GetResults();
                    if (response.IsSuccessStatusCode)
                    {
                        try
                        {
                            string responseStr = await response.Content.ReadAsStringAsync();
                            Debug.WriteLine("Response to details: " + responseStr);
                            operationResult = JsonConvert.DeserializeObject<SynchronizationMovementDetails>(responseStr);
                        }
                        catch (Exception)
                        {
                            operationResult = null;
                        }
                    }
                    else
                    {
                        operationResult = null;
                    }

                    response.Dispose();
                    releaseOperation = true;
                    break;
                case AsyncStatus.Canceled:
                case AsyncStatus.Error:
                    operationResult = null;
                    releaseOperation = true;
                    break;
            }

            if (releaseOperation)
            {
                await operationBarrier.SignalAndWait();

                /*lock (operationEndCondition)
                {
                    operationEnded = true;
                    Monitor.PulseAll(operationEndCondition);
                }*/
            }
        }

        private async void synchronize(IAsyncAction operation)
        {
            Debug.WriteLine("Sync thread started");
            int serverErrors = 0;

            syncThreadSpawnSem.Wait();
            if (operation.Status == AsyncStatus.Canceled) { return; }

            try
            {
                #region Movement push
                ICollection<SynchronizationMovementCreation> toBeCreatedMovements = await LocalStorageManager.getMovementCreationEvents();

                if (operation.Status == AsyncStatus.Canceled) { return; }

                if (toBeCreatedMovements != null && toBeCreatedMovements.Count != 0)
                {
                    foreach (SynchronizationMovementCreation creationEvent in toBeCreatedMovements)
                    {
                        if (serverErrors > MAX_SERVER_ERRORS)
                        {
                            return;
                        }
                        LocalMovement movement = await LocalStorageManager.getMovement(creationEvent.MovementId);
                        string output = JsonConvert.SerializeObject(new AddMovementPacket
                        {
                            title = movement.Title,
                            value = movement.Value,
                            subject = movement.SubjectName,
                            date = movement.Date
                        });

                        operationResult = null;
                        operationResultEx = null;
                        //operationEnded = false;
                        operationBarrier = new AsyncBarrier(2);
                        doRequest(Method.POST, App.MOVEMENTS_URI, manageMovementAddEnd, output);

                        await operationBarrier.SignalAndWait();

                        /*lock (operationEndCondition)
                        {
                            while (!operationEnded)
                            {
                                Monitor.Wait(operationEndCondition);
                            }
                        }*/

                        httpOperation = null;

                        AddMovementResult result = (AddMovementResult)operationResult;
                        switch (result)
                        {
                            case AddMovementResult.NET_ERROR:
                                return;
                            case AddMovementResult.SERVER_ERROR:
                                serverErrors++;
                                break;
                            case AddMovementResult.CONFLICT:
                                Debug.WriteLine("Deleting movement creation event after conflict");
                                await LocalStorageManager.deleteMovementCreationPaymentsAndDoneEventsAndStorageData(movement);
                                doCallback(SynchronizationSubject.MOVEMENT_DELETED, movement);
                                break;
                            case AddMovementResult.SUCCESS:
                                Debug.WriteLine("Deleting movement creation event after success");
                                SynchronizationMovementTimestamp timestamp = (SynchronizationMovementTimestamp)operationResultEx;
                                //await LocalStorageManager.deleteMovementCreationEvent(creationEvent.Id);
                                await LocalStorageManager.updateMovementTimestampAndRemoveCreationEvent(movement, timestamp.Timestamp, creationEvent.Id);
                                break;
                        }
                        if (operation.Status == AsyncStatus.Canceled) { return; }
                    }
                }
                #endregion

                #region Movement push done/undone
                ICollection<SynchronizationMovementDone> toBeDoneMovements = await LocalStorageManager.getMovementSetDoneEvents();

                Debug.WriteLine(toBeDoneMovements.Count);

                if (operation.Status == AsyncStatus.Canceled) { return; }

                if (toBeDoneMovements != null && toBeDoneMovements.Count != 0)
                {
                    foreach (SynchronizationMovementDone doneEvent in toBeDoneMovements)
                    {
                        if (serverErrors > MAX_SERVER_ERRORS)
                        {
                            return;
                        }
                        LocalMovement movement = await LocalStorageManager.getMovement(doneEvent.MovementId);
                        string output = JsonConvert.SerializeObject(new SetDonePacket
                        {
                            done = movement.Done
                        });

                        operationResult = null;
                        operationResultEx = null;
                        //operationEnded = false;
                        operationBarrier = new AsyncBarrier(2);
                        doRequest(Method.POST, App.getSetMovementDoneUri(movement), manageMovementSetDone, output);

                        await operationBarrier.SignalAndWait();

                        /*lock (operationEndCondition)
                        {
                            while (!operationEnded)
                            {
                                Monitor.Wait(operationEndCondition);
                            }
                        }*/
                        httpOperation = null;

                        SetMovementDoneResult result = (SetMovementDoneResult)operationResult;
                        Debug.WriteLine(result.ToString());
                        switch (result)
                        {
                            case SetMovementDoneResult.NET_ERROR:
                                return;
                            case SetMovementDoneResult.SERVER_ERROR:
                                serverErrors++;
                                break;
                            case SetMovementDoneResult.NOT_FOUND:
                                await LocalStorageManager.deleteMovementCreationPaymentsAndDoneEventsAndStorageData(movement);
                                doCallback(SynchronizationSubject.MOVEMENT_DELETED, movement);
                                break;
                            case SetMovementDoneResult.SUCCESS:
                                await LocalStorageManager.deleteMovementDoneEvent(doneEvent.Id);
                                break;
                        }
                        if (operation.Status == AsyncStatus.Canceled) { return; }
                    }
                }
                #endregion

                #region Payment push to remote
                ICollection<SynchronizationPaymentCreation> toBeAddedPayments = await LocalStorageManager.getPaymentCreationEvents();

                if (operation.Status == AsyncStatus.Canceled) { return; }

                if (toBeAddedPayments != null && toBeAddedPayments.Count != 0)
                {
                    foreach (SynchronizationPaymentCreation createPaymentEvent in toBeAddedPayments)
                    {
                        if (serverErrors > MAX_SERVER_ERRORS)
                        {
                            return;
                        }
                        LocalMovement movement = await LocalStorageManager.getMovement(createPaymentEvent.MovementId);
                        LocalPayment payment = await LocalStorageManager.getPayment(createPaymentEvent.PaymentId);
                        string output = JsonConvert.SerializeObject(new AddPaymentPacket
                        {
                            value = payment.Value,
                            paymentTimestamp = payment.PaymentDate
                        });

                        operationResult = null;
                        operationResultEx = null;
                        //operationEnded = false;
                        operationBarrier = new AsyncBarrier(2);
                        doRequest(Method.POST, App.getAddPaymentUri(movement), managePaymentAdd, output);
                        await operationBarrier.SignalAndWait();

                        /*lock (operationEndCondition)
                        {
                            while (!operationEnded)
                            {
                                Monitor.Wait(operationEndCondition);
                            }
                        }*/
                        httpOperation = null;

                        AddPaymentResult result = (AddPaymentResult)operationResult;
                        switch (result)
                        {
                            case AddPaymentResult.NET_ERROR:
                                return;
                            case AddPaymentResult.SERVER_ERROR:
                                serverErrors++;
                                break;
                            case AddPaymentResult.NOT_FOUND:
                                await LocalStorageManager.deleteMovementCreationPaymentsAndDoneEventsAndStorageData(movement);
                                doCallback(SynchronizationSubject.MOVEMENT_DELETED, movement);
                                break;
                            case AddPaymentResult.CONFLICT:
                                await LocalStorageManager.deletePaymentCreationAndStorageData(payment, createPaymentEvent);
                                doCallback(SynchronizationSubject.PAYMENT_DELETED, payment);
                                break;
                            case AddPaymentResult.SUCCESS:
                                SynchronizationPaymentTimestamp timestamp = (SynchronizationPaymentTimestamp)operationResultEx;
                                await LocalStorageManager.updatePaymentTimestampAndRemoveCreationEvent(payment, timestamp.Timestamp, createPaymentEvent.Id);
                                break;
                        }
                        if (operation.Status == AsyncStatus.Canceled) { return; }
                    }
                }
                #endregion

                ICollection<LocalMovement> movements = await LocalStorageManager.getAllMovements();
                Dictionary<LocalMovement, bool> movementsDone = new Dictionary<LocalMovement, bool>();

                #region Movements get remote
                //operationEnded = false;
                operationResult = null;
                operationBarrier = new AsyncBarrier(2);
                doRequest(Method.GET, App.MOVEMENTS_TIMESTAMP_URI, manageMovementTimestamp, null);
                await operationBarrier.SignalAndWait();

                /*lock (operationEndCondition)
                {
                    while (!operationEnded)
                    {
                        Monitor.Wait(operationEndCondition);
                    }
                }*/
                httpOperation = null;

                if (operation.Status == AsyncStatus.Canceled) { return; }

                if (operationResult != null)
                {
                    List<SynchronizationMovementTimestamp> result = (List<SynchronizationMovementTimestamp>)operationResult;

                    ICollection<SynchronizationMovementTimestamp> newMovements = new LinkedList<SynchronizationMovementTimestamp>();

                    ICollection<LocalMovement> toBeDeletedMovements = new LinkedList<LocalMovement>();

                    foreach (LocalMovement localMovement in movements)
                    {
                        SynchronizationMovementTimestamp exists = result.Where(x => x.Timestamp == localMovement.Date).FirstOrDefault();
                        if( exists == null ) {
                            toBeDeletedMovements.Add( localMovement );
                        }
                    }

                    foreach (SynchronizationMovementTimestamp movementTimestamp in result)
                    {
                        LocalMovement exists = movements.Where(x => x.Date == movementTimestamp.Timestamp).FirstOrDefault();
                        if( exists == null ) {
                            newMovements.Add(movementTimestamp);
                        }
                        else if( exists != null && exists.Done != (movementTimestamp.Done != 0))
                        {
                            movementsDone.Add(exists, movementTimestamp.Done != 0);
                        }
                    }

                    foreach (SynchronizationMovementTimestamp movementTimestamp in newMovements)
                    {
                        if (serverErrors > MAX_SERVER_ERRORS)
                        {
                            return;
                        }

                        //operationEnded = false;
                        operationResult = null;
                        operationResultEx = null;
                        operationBarrier = new AsyncBarrier(2);
                        doRequest(Method.GET, App.getMovementDetailsByTimestampUri(movementTimestamp.Timestamp), manageMovementDetails, null);
                        await operationBarrier.SignalAndWait();

                        /*lock (operationEndCondition)
                        {
                            while (!operationEnded)
                            {
                                Monitor.Wait(operationEndCondition);
                            }
                        }*/
                        httpOperation = null;

                        if (operationResult != null)
                        {
                            SynchronizationMovementDetails details = (SynchronizationMovementDetails) operationResult;
                            LocalMovement mov = new LocalMovement
                            {
                                Owner = details.Owner,
                                Date = details.Timestamp,
                                Title = details.Title,
                                Value = details.Value,
                                CurrentValue = details.Value,
                                SubjectName = details.SubjectName,
                                Target = details.Target,
                                Done = details.Done
                            };
                            
                            int movementId = await LocalStorageManager.addMovement(mov);

                            mov.Id = movementId;

                            movements.Add(mov);
                            doCallback(SynchronizationSubject.MOVEMENT_CREATION, mov);
                        }
                        else
                        {
                            serverErrors++;
                        }
                    }

                    foreach (LocalMovement delMov in toBeDeletedMovements)
                    {
                        movements.Remove(delMov);
                        if (await LocalStorageManager.deleteMovement(delMov))
                        {
                            doCallback(SynchronizationSubject.MOVEMENT_DELETED, delMov);
                        }
                    }
                }
                #endregion

                #region Payments get remote
                foreach (LocalMovement movement in movements)
                {
                    Debug.WriteLine("Updating payments for movement " + movement.Date);
                    //operationEnded = false;
                    operationResult = null;
                    operationResultEx = null;
                    operationBarrier = new AsyncBarrier(2);
                    doRequest(Method.GET, App.getMovementPaymentsByTimestampUri(movement.Date), manageMovementPayments, null);
                    await operationBarrier.SignalAndWait();

                    /*lock (operationEndCondition)
                    {
                        while (!operationEnded)
                        {
                            Monitor.Wait(operationEndCondition);
                        }
                    }*/
                    httpOperation = null;

                    if (operation.Status == AsyncStatus.Canceled) { return; }

                    if (operationResult != null)
                    {
                        List<SynchronizationMovementPayment> result = (List<SynchronizationMovementPayment>)operationResult;

                        ICollection<LocalPayment> payments = await LocalStorageManager.getAllPayments(movement.Owner, movement.Date);

                        ICollection<SynchronizationMovementPayment> newPayments = new LinkedList<SynchronizationMovementPayment>();

                        ICollection<LocalPayment> toBeDeletedPayments = new LinkedList<LocalPayment>();

                        foreach (LocalPayment localPayment in payments)
                        {
                            SynchronizationMovementPayment exists = result.Where(x => x.Timestamp == localPayment.PaymentDate).FirstOrDefault();
                            if (exists == null)
                            {
                                toBeDeletedPayments.Add(localPayment);
                            }
                            Debug.WriteLine("Payment date: " + localPayment.PaymentDate);
                        }

                        foreach (SynchronizationMovementPayment paymentTimestamp in result)
                        {
                            LocalPayment exists = payments.Where(x => x.PaymentDate == paymentTimestamp.Timestamp).FirstOrDefault();
                            if (exists == null)
                            {
                                newPayments.Add(paymentTimestamp);
                            }
                        }

                        foreach (SynchronizationMovementPayment payment in newPayments)
                        {
                            LocalPayment pay = new LocalPayment {
                                Value = payment.Value,
                                MovementDate = movement.Date,
                                OwnerUsername = movement.Owner,
                                PaymentDate = payment.Timestamp
                            };

                            int paymentId = await LocalStorageManager.addPaymentAndUpdateMovement(pay);

                            pay.Id = paymentId;

                            doCallback(SynchronizationSubject.PAYMENT_CREATION, pay);
                        }

                        foreach (LocalPayment delPay in toBeDeletedPayments)
                        {
                            if (await LocalStorageManager.deletePayment(delPay))
                            {
                                doCallback(SynchronizationSubject.PAYMENT_DELETED, delPay);
                            }
                        }
                    }
                    else
                    {
                        serverErrors++;
                    }
                }
                #endregion

                #region Done get remote
                foreach (KeyValuePair<LocalMovement, bool> doneEntry in movementsDone)
                {
                    Debug.WriteLine("Updating done for movement " + doneEntry.Key);

                    doneEntry.Key.Done = doneEntry.Value;
                    if (await LocalStorageManager.updateMovement(doneEntry.Key))
                    {
                        doCallback(SynchronizationSubject.MOVEMENT_CHANGE, doneEntry.Key);
                    }
                }
                #endregion

            }
            finally
            {
                syncThreadSpawnSem.Release();
            }
        }

        private enum AddMovementResult
        {
            NET_ERROR, SERVER_ERROR, CONFLICT, SUCCESS
        }

        private enum SetMovementDoneResult
        {
            NET_ERROR, SERVER_ERROR, NOT_FOUND, SUCCESS
        }

        private enum AddPaymentResult
        {
            NET_ERROR, SERVER_ERROR, NOT_FOUND, CONFLICT, SUCCESS
        }

        private enum Method
        {
            GET, POST
        }

        private void doRequest(Method method, String url,
            AsyncOperationWithProgressCompletedHandler<HttpResponseMessage, HttpProgress> callback, string jsonBody = null)
        {
            UserData data = app.getUserData();
            if (data == null)
            {
                return;
            }

            var RootFilter = new HttpBaseProtocolFilter();
            RootFilter.CacheControl.ReadBehavior = Windows.Web.Http.Filters.HttpCacheReadBehavior.MostRecent;
            RootFilter.CacheControl.WriteBehavior = Windows.Web.Http.Filters.HttpCacheWriteBehavior.NoCache;

            Windows.Web.Http.HttpClient client = new HttpClient(RootFilter);


            var buffer = Windows.Security.Cryptography.CryptographicBuffer.ConvertStringToBinary(data.Username + ":" + data.Password, Windows.Security.Cryptography.BinaryStringEncoding.Utf8);
            string base64token = Windows.Security.Cryptography.CryptographicBuffer.EncodeToBase64String(buffer);
            client.DefaultRequestHeaders.Authorization = new HttpCredentialsHeaderValue("Basic", base64token);
            client.DefaultRequestHeaders.Add(new KeyValuePair<string, string>("Accept", "application/json"));

            if (method == Method.POST)
            {
                HttpStringContent stringContent = new HttpStringContent(jsonBody, UnicodeEncoding.Utf8, "application/json");
                httpOperation = client.PostAsync(new Uri(url), stringContent);
            }
            else
            {
                httpOperation = client.GetAsync(new Uri(url));
            }

            httpOperation.Completed += callback;
        }

        private void doCallback(SynchronizationSubject subject, object data) {
            if( OnSynchronized != null ) {
                OnSynchronized(subject, data);
            }
        }   
    }
}
