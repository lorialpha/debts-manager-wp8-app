﻿using Newtonsoft.Json;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager
{
    [DataContract]
    public class LocalMovement
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [DataMember(Name="owner")]
        public string Owner { get; set; }

        [DataMember(Name = "date")]
        public string Date { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "value")]
        public double Value { get; set; }

        [DataMember(Name = "current_value")]
        public double CurrentValue { get; set; }

        [DataMember(Name = "subject_name")]
        public string SubjectName { get; set; }

        [DataMember(Name = "target")]
        public string Target { get; set; }

        [DataMember(Name = "done")]
        public bool Done { get; set; }

        /*[DataMember(Name = "synchronized")]
        public bool Synchronized { get; set; }*/
    }
}
