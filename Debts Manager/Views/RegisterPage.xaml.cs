﻿using Debts_Manager.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Debts_Manager.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class RegisterPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private volatile IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> httpOperation = null;
        private volatile string currentHttpRegisterUsername = null;

        public RegisterPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            UsernameErrorLabel.Visibility = Visibility.Collapsed;
            PasswordErrorLabel.Visibility = Visibility.Collapsed;
            NameErrorLabel.Visibility = Visibility.Collapsed;
            SurnameErrorLabel.Visibility = Visibility.Collapsed;
            MailErrorLabel.Visibility = Visibility.Collapsed;
            ConfirmPasswordErrorLabel.Visibility = Visibility.Collapsed;
            RegisterConnectionErrorLabel.Visibility = Visibility.Collapsed;
            RegisterUsernameAlreadyInUseLabel.Visibility = Visibility.Collapsed;
            RegisterGenericErrorLabel.Visibility = Visibility.Collapsed;

            UI_hideProgress();
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string username = e.Parameter as string;
            if ( username != null)
            {
                username_input.Text = username;
            }
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            UI_closeHttpRequest();
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void UI_closeHttpRequest() {
            if (httpOperation != null)
            {
                httpOperation.Cancel();
                httpOperation = null;
            }
        }

        private void register_button_Click(object sender, RoutedEventArgs e)
        {
            setHintVisibility(RegisterConnectionErrorLabel, true);
            setHintVisibility(RegisterUsernameAlreadyInUseLabel, true);
            setHintVisibility(RegisterGenericErrorLabel, true);

            if (checkInputs())
            {
                if (register(username_input.Text.Trim(), password_input.Password, name_input.Text.Trim(), surname_input.Text.Trim(), mail_input.Text.Trim()))
                {
                    startProgress();
                    currentHttpRegisterUsername = username_input.Text;
                    var later = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UI_disableInputs());
                }
            }
            
        }

        private bool checkInputs()
        {
            string username = username_input.Text;
            string password = password_input.Password;
            string confirm_password = password_confirm_input.Password;
            string name = name_input.Text;
            string surname = surname_input.Text;
            string mail = mail_input.Text;

            username = username.Trim();
            name = name.Trim();
            surname = surname.Trim();
            mail = mail.Trim();
            bool globalCheck;
            bool usernameLengthCheck = username.Length >= 4;
            bool passwordLengthCheck = password.Length >= 8;
            bool nameLengthCheck = name.Length >= 2;
            bool surnameLengthCheck = surname.Length >= 2;
            bool mailCheck = App.isValidEmail(mail);
            bool passwordConfirmCheck = password.Equals(confirm_password, StringComparison.Ordinal);
            globalCheck = passwordLengthCheck && usernameLengthCheck && nameLengthCheck && surnameLengthCheck && mailCheck && passwordConfirmCheck;

            setHintVisibility(UsernameErrorLabel, usernameLengthCheck);
            setHintVisibility(PasswordErrorLabel, passwordLengthCheck);
            setHintVisibility(NameErrorLabel, nameLengthCheck);
            setHintVisibility(SurnameErrorLabel, surnameLengthCheck);
            setHintVisibility(MailErrorLabel, mailCheck);

            if (passwordLengthCheck)
            {
                setHintVisibility(ConfirmPasswordErrorLabel, passwordConfirmCheck);
            }

            return globalCheck;
        }

        private void setHintVisibility(TextBlock block, bool check)
        {
            if (check)
            {
                block.Visibility = Visibility.Collapsed;
            }
            else
            {
                block.Visibility = Visibility.Visible;
            }
        }

        private void setHintVisibility(TextBlock block)
        {
            block.Visibility = Visibility.Visible;
        }

        private void afterRegistration()
        {
            UI_closeHttpRequest();
            (App.Current as App).LastUsername = currentHttpRegisterUsername;

            if (Frame.CanGoBack)
            {
                Frame.GoBack();
            }
        }

        private void afterRegistrationFail()
        {
            UI_closeHttpRequest();
            UI_hideProgress();
            currentHttpRegisterUsername = null;
            register_button.IsEnabled = true;
            UI_enableInputs();
        }

        private async void registerHandler(IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> asyncInfo, AsyncStatus asyncStatus)
        {
            IAsyncAction ignored;
            switch (asyncStatus)
            {
                case AsyncStatus.Error:
                    ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(RegisterConnectionErrorLabel));
                    ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => afterRegistrationFail());
                    break;
                case AsyncStatus.Canceled:
                    break;
                case AsyncStatus.Started:
                    break;
                case AsyncStatus.Completed:
                    HttpResponseMessage response = asyncInfo.GetResults();
                    if (response.IsSuccessStatusCode)
                    {
                        //Codice 201
                        response.Dispose();
                        ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => afterRegistration());
                    }
                    else
                    {
                        bool isUnknownError = true;
                        if( response.StatusCode == HttpStatusCode.Conflict )
                        {
                            //Username già in uso
                            isUnknownError = false;
                            response.Dispose();
                            ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(RegisterUsernameAlreadyInUseLabel));
                        }
                        else if (response.StatusCode == HttpStatusCode.BadRequest )
                        {
                            //Bad request
                            string result = await response.Content.ReadAsStringAsync();
                            response.Dispose();
                            try
                            {
                                RegisterBadInputBundle bundle = (RegisterBadInputBundle)JsonConvert.DeserializeObject(result, typeof(RegisterBadInputBundle));

                                if (bundle != null)
                                {
                                    ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(UsernameErrorLabel, bundle.username));
                                    ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(PasswordErrorLabel, bundle.password));
                                    ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(NameErrorLabel, bundle.name));
                                    ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(SurnameErrorLabel, bundle.surname));
                                    ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(MailErrorLabel, bundle.mail));
                                    isUnknownError = false;
                                }
                            }
                            catch (Newtonsoft.Json.JsonReaderException) {}
                        }
                        else
                        {
                            response.Dispose();
                        }

                        if (isUnknownError)
                        {
                            ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(RegisterGenericErrorLabel));
                        }

                        ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => afterRegistrationFail());
                    }
                    break;
            }
        }

        private bool register(string username, string password, string name, string surname, string mail)
        {
            if ((App.Current as App).isConnectionAvailable())
            {
                HttpClient client = new HttpClient();
                
                string postData = JsonConvert.SerializeObject(new {username= username, password= password, name= name, surname= surname, mail= mail});
                HttpStringContent stringContent = new HttpStringContent(postData, UnicodeEncoding.Utf8, "application/json");
                httpOperation = client.PostAsync(new Uri(App.REGISTER_URI), stringContent);
                httpOperation.Completed += registerHandler;

                return true;
            }
            else
            {
                RegisterConnectionErrorLabel.Visibility = Visibility.Visible;
                return false;
            }
        }

        private void UI_disableInputs()
        {
            username_input.IsEnabled = false;
            password_input.IsEnabled = false;
            password_confirm_input.IsEnabled = false;
            name_input.IsEnabled = false;
            surname_input.IsEnabled = false;
            mail_input.IsEnabled = false;
            register_button.IsEnabled = false;
        }

        private void UI_enableInputs()
        {
            username_input.IsEnabled = true;
            password_input.IsEnabled = true;
            password_confirm_input.IsEnabled = true;
            name_input.IsEnabled = true;
            surname_input.IsEnabled = true;
            mail_input.IsEnabled = true;
            register_button.IsEnabled = true;
        }

        private async void startProgress()
        {
            await Task.Delay(TimeSpan.FromMilliseconds(1000));
            var ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UI_showProgress());
        }

        private void UI_showProgress()
        {
            if (httpOperation != null)
            {
                Progress.Visibility = Visibility.Visible;
            }
            else
            {
                Progress.Visibility = Visibility.Collapsed;
            }
        }

        private void UI_hideProgress()
        {
            Progress.Visibility = Visibility.Collapsed;
        }

        private class RegisterBadInputBundle
        {
            public bool name { get; set; }
            public bool surname { get; set; }
            public bool mail { get; set; }
            public bool username { get; set; }
            public bool password { get; set; }

        }
    }

}
