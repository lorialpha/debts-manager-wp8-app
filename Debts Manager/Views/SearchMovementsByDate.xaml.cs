﻿using Debts_Manager.Common;
using Debts_Manager.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace Debts_Manager.Views
{
    public sealed partial class SearchMovementsByDate : Page
    {
        private DateTime startDate, endDate;
        private List<OverviewMovement> movements;
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private SemaphoreSlim updateSemaphore = new SemaphoreSlim(1);

        public SearchMovementsByDate()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
            movements = new List<OverviewMovement>();

            MovementsList.DataContext = movements;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            string startDateStr = null;
            string endDateStr = null;
            if (e.PageState != null && e.PageState.ContainsKey("StartDate"))
            {
                startDateStr = e.PageState["StartDate"] as string;
                endDateStr = e.PageState["EndDate"] as string;
            }

            if (startDateStr != null && endDateStr != null)
            {
                DateTime a = App.ParseDateString(startDateStr).ToLocalTime();
                DateTime b = App.ParseDateString(endDateStr).ToLocalTime();

                StartDatePicker.Date = new DateTimeOffset(a);
                EndDatePicker.Date = new DateTimeOffset(b);

                var later = loadMovements(a, b);
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            e.PageState["StartDate"] = App.DateTimeToDateString(startDate);
            e.PageState["EndDate"] = App.DateTimeToDateString(endDate);
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            StartDatePicker.Date = DateTimeOffset.Now.AddMonths(-2);
            EndDatePicker.Date = DateTimeOffset.Now;

            (App.Current as App).synchronizationManager.OnSynchronized += SynchronizationCallback;
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            (App.Current as App).synchronizationManager.OnSynchronized -= SynchronizationCallback;
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void OnPostItemClick(object sender, ItemClickEventArgs e)
        {
            OverviewMovement mov = (OverviewMovement)e.ClickedItem;
            Frame.Navigate(typeof(MovementDetails), mov.InternalId);
        }



        private async Task<bool> loadMovements(DateTime startDate, DateTime endDate)
        {
            await updateSemaphore.WaitAsync();
            try
            {
                this.startDate = startDate;
                this.endDate = endDate;
                ICollection<LocalMovement> result = await LocalStorageManager.getMovementsWithDate(startDate, endDate);

                if (result == null)
                {
                    return false;
                }

                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => updateMovements(result));

                return true;
            }
            finally
            {
                updateSemaphore.Release();
            }
        }

        private void updateMovements(ICollection<LocalMovement> movs)
        {
            movements = new List<OverviewMovement>(movs.Count);
            foreach (LocalMovement item in movs)
            {
                movements.Add(new OverviewMovement { Title = item.Title + " - " + item.SubjectName, Date = App.ParseDateString(item.Date).ToLocalTime().ToString(), Value = item.CurrentValue.ToString("C", CultureInfo.CurrentCulture), InternalId = item.Id });
            }
            MovementsList.DataContext = movements;
        }

        private void SynchronizationCallback(Debts_Manager.SynchronizationManager.SynchronizationSubject subject, object data)
        {
            Debug.WriteLine("Received synchronization callback: " + subject);

            if (startDate == null || endDate == null)
            {
                return;
            }

            string startStr = App.DateTimeToDateString(startDate);
            string endStr = App.DateTimeToDateString(endDate);

            switch (subject)
            {
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.MOVEMENT_CREATION:
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.MOVEMENT_CHANGE:
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.MOVEMENT_DELETED:
                    if (((LocalMovement)data).Date.CompareTo(startStr) >= 0 && ((LocalMovement)data).Date.CompareTo(endStr) <= 0)
                    {
                        var later = loadMovements(startDate, endDate);
                    }
                    break;
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.PAYMENT_CREATION:
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.PAYMENT_DELETED:
                    if (((LocalPayment)data).MovementDate.CompareTo(startStr) >= 0 && ((LocalPayment)data).MovementDate.CompareTo(endStr) <= 0)
                    {
                        var later = loadMovements(startDate, endDate);
                    }
                    break;
            }
        }

        private void search_button_Click(object sender, RoutedEventArgs e)
        {
            DateTime start = StartDatePicker.Date.DateTime;
            DateTime end = EndDatePicker.Date.DateTime;

            end.AddDays(1);
            end.AddSeconds(-1);

            if (start > end)
            {
                EndDatePicker.Date = StartDatePicker.Date;
                end = start;
                end.AddDays(1);
                end.AddSeconds(-1);
            }

            var later = loadMovements(start, end);
        }
    }
}
