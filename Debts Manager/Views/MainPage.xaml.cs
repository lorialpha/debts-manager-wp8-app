﻿using Debts_Manager.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;
using Windows.Web.Http.Headers;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Debts_Manager.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private volatile IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> httpOperation = null;
        private string currentHttpLoginPassword = null;
        private bool isFirstTry = false;

        public MainPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            UI_hideErrorLabels();
            UI_hideProgress();

            UserData user_data = (App.Current as App).getUserData();

            if (user_data != null)
            {
                //username_input.Text = user_data.Username;
                isFirstTry = true;
                if (login(user_data.Username, user_data.Password))
                {
                    startProgress();
                    username_input.Text = user_data.Username;
                    username_input.IsEnabled = false;
                    password_input.IsEnabled = false;
                    register_button.IsEnabled = false;
                    login_button.IsEnabled = false;
                }
                else
                {
                    //Did login succesfully but no connection at the moment, so proceed offline
                    UI_proceedOffline();
                }
            }
            else if ((App.Current as App).LastUsername != null)
            {
                username_input.Text = (App.Current as App).LastUsername;
            }
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            UI_hideErrorLabels();
            if ((App.Current as App).LastUsername != null)
            {
                username_input.Text = (App.Current as App).LastUsername;
            }
            else if (e.PageState != null)
            {
                object username = null;
                if (e.PageState.TryGetValue("username", out username))
                {
                    string val = username as string;
                    if (val != null)
                    {
                        username_input.Text = val;
                    }
                }
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            e.PageState["username"] = username_input.Text;
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            Frame.BackStack.Clear();
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void register_button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(RegisterPage), username_input.Text);
        }

        private void login_button_Click(object sender, RoutedEventArgs e)
        {
            UI_hideErrorLabels();
            string username = username_input.Text;
            string password = password_input.Password;
            username = username.Trim();
            if( username.Length == 0) {
                LoginWrongCredentialsLabel.Visibility = Visibility.Visible;
            }
            else if (login(username, password))
            {
                startProgress();
                username_input.IsEnabled = false;
                password_input.IsEnabled = false;
                register_button.IsEnabled = false;
                login_button.IsEnabled = false;
            }
            else
            {
                LoginConnectionErrorLabel.Visibility = Visibility.Visible;
            }
        }

        private void afterLoginFail(bool wrongCredentials) {
            (App.Current as App).deleteUserData();
            (App.Current as App).setLastLoggedUser(null);
            username_input.IsEnabled = true;
            password_input.IsEnabled = true;
            register_button.IsEnabled = true;
            login_button.IsEnabled = true;
            currentHttpLoginPassword = null;
            httpOperation = null;
            if( wrongCredentials ) {
                password_input.Password = "";
            }
            UI_hideProgress();
        }

        private async void afterLogin(UserData user_data)
        {
            string lastLogged = (App.Current as App).getLastLoggedUser();
            if( lastLogged == null || ! user_data.Username.Equals(lastLogged) ) {
                await (App.Current as App).synchronizationManager.recreateStorage();
            }
            
            (App.Current as App).storeUserData(user_data);
            (App.Current as App).setLastLoggedUser(user_data.Username);
            Debug.WriteLine("Before sync start");
            (App.Current as App).synchronizationManager.startSync();
            Debug.WriteLine("After sync start");
            var ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { Frame.Navigate(typeof(Overview)); });
        }

        private async void loginHandler(IAsyncOperationWithProgress<HttpResponseMessage, HttpProgress> asyncInfo, AsyncStatus asyncStatus)
        {
            IAsyncAction ignored;
            switch (asyncStatus)
            {
                case AsyncStatus.Error:
                    if (isFirstTry)
                    {
                        ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UI_proceedOffline());
                    }
                    else
                    {
                        ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(LoginConnectionErrorLabel));
                        ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => afterLoginFail(false));
                    }
                    break;
                case AsyncStatus.Canceled:
                    break;
                case AsyncStatus.Started:
                    break;
                case AsyncStatus.Completed:
                    HttpResponseMessage response = asyncInfo.GetResults();
                    if (response.IsSuccessStatusCode)
                    {
                        //Codice 200
                        string result = await response.Content.ReadAsStringAsync();

                        UserData user_data = null;
                        try
                        {
                            user_data = (UserData)JsonConvert.DeserializeObject(result, typeof(UserData));
                        }
                        catch (Newtonsoft.Json.JsonReaderException e) {
                            Debug.WriteLine(e);
                        }

                        if (user_data == null)
                        {
                            ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(LoginGenericErrorLabel));
                            ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => afterLoginFail(false));
                        }
                        else
                        {
                            user_data.Password = currentHttpLoginPassword;
                            afterLogin(user_data);
                        }
                    }
                    else
                    {
                        if ((int)response.StatusCode >= 500 && (int)response.StatusCode < 600)
                        {
                            if (isFirstTry)
                            {
                                ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UI_proceedOffline());
                            }
                            else
                            {
                                ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(LoginGenericErrorLabel));
                                ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => afterLoginFail(false));
                            }
                        }
                        else
                        {
                            ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => setHintVisibility(LoginWrongCredentialsLabel));
                            ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => afterLoginFail(true));
                        }
                    }

                    response.Dispose();
                    break;
            }
            ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => { isFirstTry = false; });
        }

        private bool login(string username, string password)
        {
            if ((App.Current as App).isConnectionAvailable())
            {
                currentHttpLoginPassword = password;
                Windows.Web.Http.HttpClient client = new HttpClient();

                var buffer = Windows.Security.Cryptography.CryptographicBuffer.ConvertStringToBinary(username + ":" + password, Windows.Security.Cryptography.BinaryStringEncoding.Utf8);
                string base64token = Windows.Security.Cryptography.CryptographicBuffer.EncodeToBase64String(buffer);
                client.DefaultRequestHeaders.Authorization = new HttpCredentialsHeaderValue("Basic", base64token);
                client.DefaultRequestHeaders.Add(new KeyValuePair<string, string>("Accept", "application/json"));

                httpOperation = client.GetAsync(new Uri(App.USER_DATA_URI));
                httpOperation.Completed += loginHandler;

                return true;
            }
            else
            {
                LoginConnectionErrorLabel.Visibility = Visibility.Visible;
                return false;
            }
        }

        private void UI_hideErrorLabels()
        {
            LoginWrongCredentialsLabel.Visibility = Visibility.Collapsed;
            LoginConnectionErrorLabel.Visibility = Visibility.Collapsed;
            LoginGenericErrorLabel.Visibility = Visibility.Collapsed;
        }

        private void setHintVisibility(TextBlock block, bool check)
        {
            if (check)
            {
                block.Visibility = Visibility.Collapsed;
            }
            else
            {
                block.Visibility = Visibility.Visible;
            }
        }

        private void setHintVisibility(TextBlock block)
        {
            block.Visibility = Visibility.Visible;
        }

        private async void startProgress()
        {
            await Task.Delay(TimeSpan.FromMilliseconds(1000));
            var ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UI_showProgress());
        }

        private void UI_showProgress()
        {
            if (httpOperation != null)
            {
                Progress.Visibility = Visibility.Visible;
            }
            else
            {
                Progress.Visibility = Visibility.Collapsed;
            }
        }

        private void UI_hideProgress()
        {
            Progress.Visibility = Visibility.Collapsed;
        }

        private void UI_proceedOffline()
        {
            Frame.Navigate(typeof(Overview));
        }
    }
}
