﻿using Debts_Manager.Common;
using Debts_Manager.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Debts_Manager.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DoneMovements : Page
    {
        private ICollection<OverviewMovement> movements;
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private SemaphoreSlim updateSemaphore = new SemaphoreSlim(1);

        public DoneMovements()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
            movements = new List<OverviewMovement>();

            MovementsList.DataContext = movements;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            var later = loadMovements();
            (App.Current as App).synchronizationManager.OnSynchronized += SynchronizationCallback;
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            (App.Current as App).synchronizationManager.OnSynchronized -= SynchronizationCallback;
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private void OnPostItemClick(object sender, ItemClickEventArgs e)
        {
            OverviewMovement mov = (OverviewMovement)e.ClickedItem;
            Frame.Navigate(typeof(DoneMovementDetails), mov.InternalId);
        }

        private void SynchronizeButton_Click(object sender, RoutedEventArgs e)
        {
            (App.Current as App).synchronizationManager.startSync();
        }

        private async Task<bool> loadMovements()
        {
            await updateSemaphore.WaitAsync();
            try { 
                ICollection<LocalMovement> result = await LocalStorageManager.getDoneMovements();

                if (result == null)
                {
                    return false;
                }

                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => updateMovements(result));

                return true;
                }
            finally
            {
                updateSemaphore.Release();
            }
        }

        private void updateMovements(ICollection<LocalMovement> movs)
        {
            movements = new List<OverviewMovement>(movs.Count);
            foreach (LocalMovement item in movs)
            {
                movements.Add(new OverviewMovement { Title = item.Title + " - " + item.SubjectName, Date = DateTime.Parse(item.Date).ToLocalTime().ToString(), Value = item.CurrentValue.ToString("C", CultureInfo.CurrentCulture), InternalId = item.Id });
            }
            MovementsList.DataContext = movements;
        }

        private void SynchronizationCallback(Debts_Manager.SynchronizationManager.SynchronizationSubject subject, object data)
        {
            Debug.WriteLine("Received synchronization callback: " + subject);
            switch (subject)
            {
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.MOVEMENT_CREATION:
                    if (((LocalMovement)data).Done == true)
                    {
                        var later = loadMovements();
                    }
                    break;
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.MOVEMENT_CHANGE:
                    {
                        var later = loadMovements();
                    }
                    break;
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.MOVEMENT_DELETED:
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.PAYMENT_CREATION:
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.PAYMENT_DELETED:
                    {
                        var later2 = loadMovements();
                    }
                    break;
            }
        }
    }
}
