﻿using Debts_Manager.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Core;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Debts_Manager.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddPaymentPage : Page
    {
        private int MovementId;
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public AddPaymentPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            UI_hideErrorLabels();
            GaveMoneyRadio.IsChecked = true;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            int? movementId = e.Parameter as int?;
            if (!movementId.HasValue)
            {
                throw new ArgumentException("No movement id was provided");
            }

            MovementId = movementId.Value;

            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void AddPaymentButton_Click(object sender, RoutedEventArgs e)
        {
            UI_hideErrorLabels();
            string value = ValueInput.Text.Trim();
            bool gave = GaveMoneyRadio.IsChecked.HasValue && GaveMoneyRadio.IsChecked.Value;
            bool success = true;
            double val;
            if (value.Length == 0)
            {
                PaymentVoidValueLabel.Visibility = Visibility.Visible;
                success = false;
            }

            try
            {
                val = double.Parse(value);
                val = Math.Round(val, 2);
            }
            catch (Exception)
            {
                PaymentBadValueLabel.Visibility = Visibility.Visible;
                success = false;
                val = 0.0;
            }

            if (val <= 0.0)
            {
                PaymentBadValueLabel.Visibility = Visibility.Visible;
                success = false;
            }

            if (!success)
            {
                return;
            }

            if (!gave)
            {
                val = -val;
            }

            /*LocalMovement movement = await LocalStorageManager.getMovement(MovementId);
            if (movement != null)
            {
                if (await LocalStorageManager.addPayment(new LocalPayment
                {
                    Value = val,
                    MovementDate = movement.Date,
                    OwnerUsername = (App.Current as App).getUserData().Username,
                    PaymentDate = DateTime.Now/*,
                    Synchronized = false
                }))
                {
                    movement.CurrentValue += val;
                    await LocalStorageManager.updateMovement(movement);
                }
            }*/

            await (App.Current as App).synchronizationManager.AddPayment(MovementId, val, DateTime.Now);

            await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (NavigationHelper.CanGoBack())
                {
                    NavigationHelper.GoBack();
                }
            });
        }

        private void UI_hideErrorLabels()
        {
            PaymentVoidValueLabel.Visibility = Visibility.Collapsed;
            PaymentBadValueLabel.Visibility = Visibility.Collapsed;
        }
    }
}
