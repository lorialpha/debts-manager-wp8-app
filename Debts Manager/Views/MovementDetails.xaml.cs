﻿using Debts_Manager.Common;
using Debts_Manager.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace Debts_Manager.Views
{
    public sealed partial class MovementDetails : Page
    {
        private int MovementId;
        private LocalMovement movement;
        private ICollection<DetailsPayment> payments;
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public MovementDetails()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;

            this.payments = new List<DetailsPayment>();
            PaymentsList.DataContext = payments;

            NoPaymentsHint.Visibility = Visibility.Collapsed;
            UI_hideProgress();
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            int? movementId = e.Parameter as int?;
            if (!movementId.HasValue)
            {
                throw new ArgumentException("No movement id was provided");
            }

            
            MovementId = movementId.Value;
            loadMovementData();
            (App.Current as App).synchronizationManager.OnSynchronized += SynchronizationCallback;
            this.navigationHelper.OnNavigatedTo(e);

            
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            (App.Current as App).synchronizationManager.OnSynchronized -= SynchronizationCallback;
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        private async void loadMovementData()
        {
            LocalMovement loadedMovement = await LocalStorageManager.getMovement(MovementId);
            if (loadedMovement == null)
            {
                var ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => {
                    if (NavigationHelper.CanGoBack())
                    {
                        NavigationHelper.GoBack();
                    }
                });
            }
            else
            {
                await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UI_setMovementData(loadedMovement));
                await loadPaymentsData(loadedMovement);
            }
        }

        private async Task loadPaymentsData(LocalMovement loadedMovement)
        {
            ICollection<LocalPayment> loadedPayments = await LocalStorageManager.getAllPayments(loadedMovement.Owner, loadedMovement.Date);
            if (loadedPayments == null)
            {
                var later = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                {
                    if (NavigationHelper.CanGoBack())
                    {
                        NavigationHelper.GoBack();
                    }
                });
            }
            else
            {
                var later = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UI_setPaymentsData(loadedPayments));
            }
        }

        private void UI_setMovementData(LocalMovement loadedMovement) {
            movement = loadedMovement;
            MovementSubject.Text = movement.SubjectName;
            MovementTitle.Text = movement.Title;
            MovementDate.Text = App.ParseDateString(movement.Date).ToLocalTime().ToString();
            MovementCurrentValue.Text = movement.CurrentValue.ToString("C", CultureInfo.CurrentCulture);
        }

        private void UI_setPaymentsData(ICollection<LocalPayment> loadedPayments)
        {
            this.payments = new List<DetailsPayment>(loadedPayments.Count);
            if (loadedPayments.Count != 0)
            {
                foreach (LocalPayment payment in loadedPayments)
                {
                    this.payments.Add(new DetailsPayment { Value = payment.Value.ToString("C", CultureInfo.CurrentCulture), Date = App.ParseDateString(payment.PaymentDate).ToLocalTime().ToString(), InternalId = payment.Id });
                }
            }
            else
            {
                NoPaymentsHint.Visibility = Visibility.Visible;
            }

            PaymentsList.DataContext = this.payments;
        }

        private void AddPaymentButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(AddPaymentPage), MovementId);
        }

        private async void SetDoneMovementButton_Click(object sender, RoutedEventArgs e)
        {
            startProgress();
            await (App.Current as App).synchronizationManager.SetMovementDone(MovementId, true);

            var later = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                if (NavigationHelper.CanGoBack())
                {
                    NavigationHelper.GoBack();
                }
            });
        }

        private async void startProgress()
        {
            await Task.Delay(TimeSpan.FromMilliseconds(1000));
            var ignored = Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => UI_showProgress());
        }

        private void UI_showProgress()
        {
            Progress.Visibility = Visibility.Visible;
        }

        private void UI_hideProgress()
        {
            Progress.Visibility = Visibility.Collapsed;
        }

        private void SynchronizationCallback(Debts_Manager.SynchronizationManager.SynchronizationSubject subject, object data)
        {
            Debug.WriteLine("Received synchronization callback: " + subject);
            switch (subject)
            {
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.MOVEMENT_CREATION:
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.MOVEMENT_CHANGE:
                    if( ((LocalMovement)data).Id == MovementId )
                    {
                        loadMovementData();
                    }
                    break;
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.MOVEMENT_DELETED:
                    if( ((LocalMovement)data).Id == MovementId )
                    {
                        if (NavigationHelper.CanGoBack())
                        {
                            NavigationHelper.GoBack();
                        }
                    }
                    break;
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.PAYMENT_CREATION:
                case Debts_Manager.SynchronizationManager.SynchronizationSubject.PAYMENT_DELETED:
                    if (movement != null && ((LocalPayment)data).MovementDate.Equals(movement.Date))
                    {
                        loadMovementData();
                    }
                    break;
            }
        }
    }
}
