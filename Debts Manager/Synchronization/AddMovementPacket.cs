﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager.Synchronization
{

    public class AddMovementPacket
    {
        public string title { get; set; }

        public double value { get; set; }

        public string subject { get; set; }

        public string date { get; set; }
    }
}
