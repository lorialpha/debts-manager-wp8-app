﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager.Synchronization
{
    public class SynchronizationMovementDetails
    {
        public string Owner { get; set; }

        public string Timestamp { get; set; }

        public string Title { get; set; }

        public double Value { get; set; }

        public double CurrentValue { get; set; }

        public string SubjectName { get; set; }

        public string Target { get; set; }

        public bool Done { get; set; }
    }
}
