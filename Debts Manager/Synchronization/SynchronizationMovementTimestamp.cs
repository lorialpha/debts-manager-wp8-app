﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager.Synchronization
{
    public class SynchronizationMovementTimestamp
    {
        public string Timestamp { get; set; }

        public int Done { get; set; }
    }
}
