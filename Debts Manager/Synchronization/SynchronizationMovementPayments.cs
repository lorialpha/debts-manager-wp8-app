﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager.Synchronization
{
    public class SynchronizationMovementPayment
    {

        public double Value { get; set; }

        public string Timestamp { get; set; }
    }
}
