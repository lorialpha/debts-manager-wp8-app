﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager.Synchronization
{
    public class SynchronizationMovementDone
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        public int MovementId { get; set; }
    }
}
