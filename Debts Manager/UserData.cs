﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager
{
    public class UserData
    {
        [JsonProperty("username")]
        public string Username {get; set;}

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("surname")]
        public string Surname { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        public UserData()
        {
            Username = null;
            Name = null;
            Surname = null;
            Password = null;
        }

        public UserData( UserData other )
        {
            Username = other.Username;
            Name = other.Name;
            Surname = other.Surname;
            Password = other.Password;
        }
    }
}
