﻿using Debts_Manager.Common;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.Connectivity;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace Debts_Manager
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : Application
    {
        public const string BASE_URI = "https://immense-island-5488.herokuapp.com";//"http://10.244.101.203:5000";//;
        public const string REST_URI = BASE_URI + "/rest";
        public const string REGISTER_URI = REST_URI + "/users";
        public const string USER_DATA_URI = REST_URI + "/user";
         
        public const string MOVEMENTS_URI = REST_URI + "/movements";
        public const string ADD_MOVEMENT_URI = MOVEMENTS_URI + "";
        public const string SET_DONE_MOVEMENT_URI = MOVEMENTS_URI + "/{timestamp}/done";
        public const string ADD_PAYMENT_URI = MOVEMENTS_URI + "/{timestamp}/payments";
        public const string MOVEMENTS_TIMESTAMP_URI = MOVEMENTS_URI + "/timestamps";
        public const string MOVEMENT_DETAILS_BYTIMESTAMP_URI = MOVEMENTS_URI + "/bytimestamp/{timestamp}";
        public const string MOVEMENT_PAYMENTS_BYTIMESTAMP_URI = MOVEMENTS_URI + "/{timestamp}/payments";

        private const string USERNAME_STORAGE_KEY = "username";
        private const string PASSWORD_STORAGE_KEY = "password";
        private const string NAME_STORAGE_KEY = "name";
        private const string SURNAME_STORAGE_KEY = "surname";
        private const string LAST_USERNAME_STORAGE_KEY = "lastusername";

        public string LastUsername { get; set; }

        private UserData CachedUserData { get; set; }

        public SynchronizationManager synchronizationManager { get; private set; }

        public void storeUserData(UserData user_data)
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            localSettings.Values[USERNAME_STORAGE_KEY] = user_data.Username;
            localSettings.Values[PASSWORD_STORAGE_KEY] = user_data.Password;
            localSettings.Values[NAME_STORAGE_KEY] = user_data.Name;
            localSettings.Values[SURNAME_STORAGE_KEY] = user_data.Surname;

            CachedUserData = new UserData(user_data);
        }

        public void deleteUserData()
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            localSettings.Values.Remove(USERNAME_STORAGE_KEY);
            localSettings.Values.Remove(PASSWORD_STORAGE_KEY);
            localSettings.Values.Remove(NAME_STORAGE_KEY);
            localSettings.Values.Remove(SURNAME_STORAGE_KEY);

            CachedUserData = null;
        }

        public UserData getUserData()
        {
            if (CachedUserData != null)
            {
                return new UserData(CachedUserData);
            }

            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            if (localSettings.Values.ContainsKey(USERNAME_STORAGE_KEY))
            {
                UserData user_data = new UserData();
                user_data.Username = (string)localSettings.Values[USERNAME_STORAGE_KEY];
                user_data.Password = (string)localSettings.Values[PASSWORD_STORAGE_KEY];
                user_data.Name = (string)localSettings.Values[NAME_STORAGE_KEY];
                user_data.Surname = (string)localSettings.Values[SURNAME_STORAGE_KEY];

                return user_data;
            }
            else
            {
                return null;
            }
        }

        public string getLastLoggedUser()
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            if (localSettings.Values.ContainsKey(LAST_USERNAME_STORAGE_KEY))
            {
                return (string)localSettings.Values[LAST_USERNAME_STORAGE_KEY];
            }

            return null;
        }

        public void setLastLoggedUser(string username)
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            localSettings.Values[LAST_USERNAME_STORAGE_KEY] = username;
        }

        public static string getSetMovementDoneUri(LocalMovement movement)
        {
            return SET_DONE_MOVEMENT_URI.Replace("{timestamp}", movement.Date);
        }

        public static string getAddPaymentUri(LocalMovement movement)
        {
            return ADD_PAYMENT_URI.Replace("{timestamp}", movement.Date);
        }

        public static string getMovementDetailsByTimestampUri(string timestamp)
        {
            return MOVEMENT_DETAILS_BYTIMESTAMP_URI.Replace("{timestamp}", timestamp);
        }

        public static string getMovementPaymentsByTimestampUri(string timestamp)
        {
            return MOVEMENT_PAYMENTS_BYTIMESTAMP_URI.Replace("{timestamp}", timestamp);
        }

        public static bool isValidEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                return (false);
        }

        public static DateTime ParseDateString(string dateStr)
        {
            return DateTime.Parse(dateStr, null, System.Globalization.DateTimeStyles.RoundtripKind);
        }

        public static string DateTimeToDateString(DateTime date)
        {
            return date.ToUniversalTime().ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
        }

        private TransitionCollection transitions;

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.LastUsername = null;
            this.CachedUserData = null;
            this.InitializeComponent();
            this.Suspending += this.OnSuspending;

            HardwareButtons.BackPressed += HardwareButton_BackPressed;
            this.synchronizationManager = new SynchronizationManager(this);
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected async override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                rootFrame.CacheSize = 2;

                SuspensionManager.RegisterFrame(rootFrame, "DebtsManager");

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    try
                    {
                        await SuspensionManager.RestoreAsync();
                    }
                    catch (Exception) { }
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // Removes the turnstile navigation for startup.
                if (rootFrame.ContentTransitions != null)
                {
                    this.transitions = new TransitionCollection();
                    foreach (var c in rootFrame.ContentTransitions)
                    {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;

                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(Views.MainPage), e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Restores the content transitions after the app has launched.
        /// </summary>
        /// <param name="sender">The object where the handler is attached.</param>
        /// <param name="e">Details about the navigation event.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            await SuspensionManager.SaveAsync();
            synchronizationManager.stopSync();

            deferral.Complete();
        }

        private void HardwareButton_BackPressed(object sender, BackPressedEventArgs e)
        {
            /*Debug.WriteLine("Go back");
            Frame frame = Window.Current.Content as Frame;

            if (frame == null)
            {
                return;
            }

            
            if (frame.CanGoBack)
            {
                Debug.WriteLine("Back : ok");
                frame.GoBack();

                e.Handled = true;
            }*/
        }

        public bool isConnectionAvailable()
        {
            try
            {

                //Ottengo il profilo di connessione
                ConnectionProfile profile = NetworkInformation.GetInternetConnectionProfile();

                //Verifico che la connessione esista
                if (profile == null)
                {
                    return false;
                }
                else
                {
                    //Verifico il livello di connettività
                    switch (profile.GetNetworkConnectivityLevel())
                    {
                        case NetworkConnectivityLevel.InternetAccess:
                            return true;
                        case NetworkConnectivityLevel.LocalAccess:
                        case NetworkConnectivityLevel.ConstrainedInternetAccess:
                        case NetworkConnectivityLevel.None:
                            return false;
                    }
                }
            }
            catch (Exception) { return false; }
            return false;
        }
    }
}