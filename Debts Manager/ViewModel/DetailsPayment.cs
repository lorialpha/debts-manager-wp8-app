﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Debts_Manager.ViewModel
{
    public class DetailsPayment
    {
        public string Date { get; set; }

        public string Value { get; set; }

        public int InternalId { get; set; }
    }
}
